package API_Schema_Validation_Automation;

import org.hamcrest.MatcherAssert;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.*;
import io.restassured.module.jsv.JsonSchemaValidator;

public class Schema_Validation {

	
	
	
	@Test(priority=1)
	public void validation_of_schemas_for_the_Author_for_Post_Api()
	{
	    // Define a JSON string representing an Author object
	    String Json ="{\r\n"
	            + "    \"id\": 0,\r\n"
	            + "    \"idBook\": 0,\r\n"
	            + "    \"firstName\": \"string\",\r\n"
	            + "    \"lastName\": \"string\"\r\n"
	            + "}" ;
	    
	    // Validate the JSON string against the JSON schema defined in the classpath
	    // Note: The schema file "Authorsecema.json"  located in Src/main/resources folder
	    MatcherAssert.assertThat(Json, JsonSchemaValidator.matchesJsonSchemaInClasspath("Authorsecema.json"));
	}
	
	// Define a test method to validate the JSON schema for the Book entity in the v1 Books Post API
	@Test(priority=2)
	public void validation_of_schemas_for_the__api_v1_Books_post_Api()
	{
	    // Define a JSON string representing a Book object
	    String Json ="{\r\n"
	            + "    \"id\": 0,\r\n"
	            + "    \"title\": \"string\",\r\n"
	            + "    \"description\": \"string\",\r\n"
	            + "    \"pageCount\": 0,\r\n"
	            + "    \"excerpt\": \"string\",\r\n"
	            + "    \"publishDate\": \"2024-04-08T08:44:09.806Z\"\r\n"
	            + "}" ;
	    
	    // Validate the JSON string against the JSON schema defined in the classpath
	    // Note: The schema file "Authorsecema.json" located in Src/main/resources folder
	    MatcherAssert.assertThat(Json, JsonSchemaValidator.matchesJsonSchemaInClasspath("Bookschema.json"));
	}
}
